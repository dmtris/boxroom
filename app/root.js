import React, { Component } from 'react';
import { connect } from 'react-redux';
import SplashsScreen from './screens/splashScreen.js';
import LoginScreen from './screens/auth/login';
// import HomeScreen from './screens/homeScreen.js';
import TabNav from './screens/main';
import SignUpScreen from './screens/auth/signup';


class Root extends Component{
    constructor(props){
        super(props);

        this.state = {
            showSplash : true,
        }

        setTimeout(() => {
            this.setState({ showSplash: false });
          }, 2500);

    }

    render(){
        return <TabNav />
        
    }
}

const mapStateToProps = function (state) {
    return{
        isAuthenticated: true
    }
};

//make this component available to the app
export default connect(mapStateToProps)(Root);
