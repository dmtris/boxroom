import React, { Component } from 'react';
import { Animated } from 'react-native';
import images from './images';

export const transitionConfig = () => {
    return {
        transitionSpec: {
            duration: 350,
            timing: Animated.timing,
            useNativeDriver: true,
        },
        screenInterpolator: sceneProps => {
            const { layout, position, scene } = sceneProps

            const thisSceneIndex = scene.index
            const width = layout.initWidth

            const translateX = position.interpolate({
                inputRange: [thisSceneIndex - 1, thisSceneIndex],
                outputRange: [width, 0],
            })

            return { transform: [{ translateX }] }
        },
    }
}

const homeListData = [{
        name: 'Boxroom App Features Video',
        view_count: 960,
        day: 2,
        image: images.image_video_cover,
    },
    {
        name: 'Boxroom App Features Video',
        view_count: 960,
        day: 2,
        image: images.image_video_cover,
    },
    {
        name: 'Boxroom App Features Video',
        view_count: 960,
        day: 2,
        image: images.image_video_cover,
    },
    {
        name: 'Boxroom App Features Video',
        view_count: 960,
        day: 2,
        image: images.image_video_cover,
    },
    {
        name: 'Boxroom App Features Video',
        view_count: 960,
        day: 2,
        image: images.image_video_cover,
    },
    {
        name: 'Boxroom App Features Video',
        view_count: 960,
        day: 2,
        image: images.image_video_cover,
    },
    {
        name: 'Boxroom App Features Video',
        view_count: 960,
        day: 2,
        image: images.image_video_cover,
    },
    {
        name: 'Boxroom App Features Video',
        view_count: 960,
        day: 2,
        image: images.image_video_cover,
    },
    {
        name: 'Boxroom App Features Video',
        view_count: 960,
        day: 2,
        image: images.image_video_cover,
    },
    {
        name: 'Boxroom App Features Video',
        view_count: 960,
        day: 2,
        image: images.image_video_cover,
    },
];

const detailVideoList = [{
        image: images.image_video_cover,
        name: 'Boxroom App Features Video',
        size: '6.25 MB',
        date: '21-07-2018',
        view_count: 960,
        day: 2,
    },
    {
        image: images.image_item_video,
        name: 'Boxroom App Features Video',
        size: '6.25 MB',
        date: '21-07-2018',
        view_count: 960,
        day: 2,
    },
    {
        image: images.image_item_video,
        name: 'Boxroom App Features Video',
        size: '6.25 MB',
        date: '21-07-2018',
        view_count: 960,
        day: 2,
    },
    {
        image: images.image_item_video,
        name: 'Boxroom App Features Video',
        size: '6.25 MB',
        date: '21-07-2018',
        view_count: 960,
        day: 2,
    },
    {
        image: images.image_item_video,
        name: 'Boxroom App Features Video',
        size: '6.25 MB',
        date: '21-07-2018',
        view_count: 960,
        day: 2,
    },
    {
        image: images.image_item_video,
        name: 'Boxroom App Features Video',
        size: '6.25 MB',
        date: '21-07-2018',
        view_count: 960,
        day: 2,
    },
    {
        image: images.image_item_video,
        name: 'Boxroom App Features Video',
        size: '6.25 MB',
        date: '21-07-2018',
        view_count: 960,
        day: 2,
    },
    {
        image: images.image_item_video,
        name: 'Boxroom App Features Video',
        size: '6.25 MB',
        date: '21-07-2018',
        view_count: 960,
        day: 2,
    },
    {
        image: images.image_item_video,
        name: 'Boxroom App Features Video',
        size: '6.25 MB',
        date: '21-07-2018',
        view_count: 960,
        day: 2,
    },
    {
        image: images.image_item_video,
        name: 'Boxroom App Features Video',
        size: '6.25 MB',
        date: '21-07-2018',
        view_count: 960,
        day: 2,
    },
    {
        image: images.image_item_video,
        name: 'Boxroom App Features Video',
        size: '6.25 MB',
        date: '21-07-2018',
        view_count: 960,
        day: 2,
    },
];

const detailMusicList = [{
        image: images.image_item_music1,
        name: 'Magents Riddim.mp3',
        size: '2.65 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_music,
        name: 'Back To You.mp3',
        size: '2.65 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_music1,
        name: 'Magents Riddim.mp3',
        size: '2.65 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_music,
        name: 'Back To You.mp3',
        size: '2.65 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_music1,
        name: 'Magents Riddim.mp3',
        size: '2.65 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_music,
        name: 'Back To You.mp3',
        size: '2.65 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_music1,
        name: 'Magents Riddim.mp3',
        size: '2.65 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_music,
        name: 'Back To You.mp3',
        size: '2.65 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_music1,
        name: 'Magents Riddim.mp3',
        size: '2.65 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_music,
        name: 'Back To You.mp3',
        size: '2.65 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_music1,
        name: 'Magents Riddim.mp3',
        size: '2.65 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_music,
        name: 'Back To You.mp3',
        size: '2.65 MB',
        date: '21-07-2018'
    },
];

const detailPictureList = [{
        image: images.image_item_picture,
        name: 'Image258745.jpg',
        size: '797 KB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_picture,
        name: 'Image258745.jpg',
        size: '797 KB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_picture,
        name: 'Image258745.jpg',
        size: '797 KB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_picture,
        name: 'Image258745.jpg',
        size: '797 KB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_picture,
        name: 'Image258745.jpg',
        size: '797 KB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_picture,
        name: 'Image258745.jpg',
        size: '797 KB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_picture,
        name: 'Image258745.jpg',
        size: '797 KB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_picture,
        name: 'Image258745.jpg',
        size: '797 KB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_picture,
        name: 'Image258745.jpg',
        size: '797 KB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_picture,
        name: 'Image258745.jpg',
        size: '797 KB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_picture,
        name: 'Image258745.jpg',
        size: '797 KB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_picture,
        name: 'Image258745.jpg',
        size: '797 KB',
        date: '21-07-2018'
    },
];

const detailDocumentList = [{
        image: images.image_item_pdf,
        name: 'Game Of Throns Part-1.pdf',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_word,
        name: 'Project Report Final.doc',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_excel,
        name: 'Finance Sheet_June.xls',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_powerpoint,
        name: 'Project Presentation.ppt',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_pdf,
        name: 'Game Of Throns Part-1.pdf',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_word,
        name: 'Project Report Final.doc',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_excel,
        name: 'Finance Sheet_June.xls',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_powerpoint,
        name: 'Project Presentation.ppt',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_pdf,
        name: 'Game Of Throns Part-1.pdf',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_word,
        name: 'Project Report Final.doc',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_excel,
        name: 'Finance Sheet_June.xls',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_powerpoint,
        name: 'Project Presentation.ppt',
        size: '1.70 MB',
        date: '21-07-2018'
    },
];

const detailAppList = [{
        image: images.icon_category_video,
        name: 'Videos',
        size: '14.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_music,
        name: 'Music',
        size: '9.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_picture,
        name: 'Gallery',
        size: '14.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_documents,
        name: 'Docs',
        size: '9.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_video,
        name: 'Videos',
        size: '14.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_music,
        name: 'Music',
        size: '9.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_picture,
        name: 'Gallery',
        size: '14.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_documents,
        name: 'Docs',
        size: '9.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_video,
        name: 'Videos',
        size: '14.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_music,
        name: 'Music',
        size: '9.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_picture,
        name: 'Gallery',
        size: '14.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_documents,
        name: 'Docs',
        size: '9.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_video,
        name: 'Videos',
        size: '14.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_music,
        name: 'Music',
        size: '9.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_picture,
        name: 'Gallery',
        size: '14.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_documents,
        name: 'Docs',
        size: '9.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_video,
        name: 'Videos',
        size: '14.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_music,
        name: 'Music',
        size: '9.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_picture,
        name: 'Gallery',
        size: '14.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_documents,
        name: 'Docs',
        size: '9.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_video,
        name: 'Videos',
        size: '14.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_music,
        name: 'Music',
        size: '9.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_picture,
        name: 'Gallery',
        size: '14.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_documents,
        name: 'Docs',
        size: '9.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_video,
        name: 'Videos',
        size: '14.2 MB',
        date: '21-07-2018'
    },
    {
        image: images.icon_category_music,
        name: 'Music',
        size: '9.2 MB',
        date: '21-07-2018'
    },
];

const detailArchiveList = [{
        image: images.image_item_archive,
        name: 'Calibri.zip',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_archive,
        name: 'Calibri.zip',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_archive,
        name: 'Calibri.zip',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_archive,
        name: 'Calibri.zip',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_archive,
        name: 'Calibri.zip',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_archive,
        name: 'Calibri.zip',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_archive,
        name: 'Calibri.zip',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_archive,
        name: 'Calibri.zip',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_archive,
        name: 'Calibri.zip',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_archive,
        name: 'Calibri.zip',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_archive,
        name: 'Calibri.zip',
        size: '1.70 MB',
        date: '21-07-2018'
    },
    {
        image: images.image_item_archive,
        name: 'Calibri.zip',
        size: '1.70 MB',
        date: '21-07-2018'
    },
];

const detailFavoriteList = [];

const detailHistoryList = [];

const detailListData = [{
        screen: 'OTHERS_DETAIL',
        title: 'Videos',
        item_count: 123,
        image: images.icon_category_video,
        data: detailVideoList,
    },
    {
        screen: 'OTHERS_DETAIL',
        title: 'Music',
        item_count: 123,
        image: images.icon_category_music,
        data: detailMusicList,
    },
    {
        screen: 'OTHERS_DETAIL',
        title: 'Pictures',
        item_count: 123,
        image: images.icon_category_picture,
        data: detailPictureList,
    },
    {
        screen: 'OTHERS_DETAIL',
        title: 'Documents',
        item_count: 123,
        image: images.icon_category_documents,
        data: detailDocumentList,
    },
    {
        screen: 'OTHERS_DETAIL',
        title: 'Apps',
        item_count: 123,
        image: images.icon_category_app,
        data: detailAppList,
    },
    {
        screen: 'OTHERS_DETAIL',
        title: 'Archive Files',
        item_count: 123,
        image: images.icon_category_archive,
        data: detailArchiveList,
    },
    {
        screen: 'OTHERS_DETAIL',
        title: 'Favorites',
        item_count: 123,
        image: images.icon_category_favorite,
        data: detailFavoriteList,
    },
    {
        screen: 'OTHERS_DETAIL',
        title: 'History',
        item_count: 123,
        image: images.icon_category_history,
        data: detailHistoryList,
    },
];

const categoryList = [{
        name: 'Files',
        screen: 'fileView',
        data: [],
    },
    {
        name: 'Videos',
        screen: 'listView',
        data: detailVideoList,
    },
    {
        name: 'Apps',
        screen: 'gridView',
        data: detailAppList,
    },
    {
        name: 'Pictures',
        screen: 'listView',
        data: detailPictureList,
    },
    {
        name: 'Music',
        screen: 'listView',
        data: detailMusicList,
    },
];

const pictureList = [{
        title: 'Mobile Wallpapers',
        type: 'wallpaper',
        data: [{
                name: 'Minimilistic wallpaper',
                image: images.image_picture_wallpaper,
            },
            {
                name: 'Minimilistic wallpaper',
                image: images.image_picture_wallpaper,
            },
            {
                name: 'Minimilistic wallpaper',
                image: images.image_picture_wallpaper,
            },
            {
                name: 'Minimilistic wallpaper',
                image: images.image_picture_wallpaper,
            },
            {
                name: 'Minimilistic wallpaper',
                image: images.image_picture_wallpaper,
            },
            {
                name: 'Minimilistic wallpaper',
                image: images.image_picture_wallpaper,
            },
        ],
    },
    {
        title: 'Top GIFs',
        type: 'gif',
        data: [{
                name: 'GIF Image',
                image: images.image_picture_gif,
            },
            {
                name: 'GIF Image',
                image: images.image_picture_gif,
            },
            {
                name: 'GIF Image',
                image: images.image_picture_gif,
            },
            {
                name: 'GIF Image',
                image: images.image_picture_gif,
            },
            {
                name: 'GIF Image',
                image: images.image_picture_gif,
            },
            {
                name: 'GIF Image',
                image: images.image_picture_gif,
            },
            {
                name: 'GIF Image',
                image: images.image_picture_gif,
            },
        ],
    },
    {
        title: 'Inspiring Quotes',
        type: 'quote',
        data: [{
                name: 'Inspring Quote',
                image: images.image_picture_wallpaper,
            },
            {
                name: 'Inspring Quote',
                image: images.image_picture_wallpaper,
            },
            {
                name: 'Inspring Quote',
                image: images.image_picture_wallpaper,
            },
            {
                name: 'Inspring Quote',
                image: images.image_picture_wallpaper,
            },
            {
                name: 'Inspring Quote',
                image: images.image_picture_wallpaper,
            },
            {
                name: 'Inspring Quote',
                image: images.image_picture_wallpaper,
            },
            {
                name: 'Inspring Quote',
                image: images.image_picture_wallpaper,
            },
            {
                name: 'Inspring Quote',
                image: images.image_picture_wallpaper,
            },
        ],
    },
];

export default constData = {
    homeListData: homeListData,
    detailListData: detailListData,
    detailVideoList: detailVideoList,
    detailMusicList: detailMusicList,
    detailPictureList: detailPictureList,
    detailDocumentList: detailDocumentList,
    detailAppList: detailAppList,
    detailArchiveList: detailArchiveList,
    detailFavoriteList: detailFavoriteList,
    detailHistoryList: detailHistoryList,
    categoryList: categoryList,
    pictureList: pictureList,
};