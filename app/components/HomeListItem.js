import React, { Component } from 'react';
import { View,ImageBackground, Image, StyleSheet, Text, TouchableOpacity } from 'react-native';
import {width,height} from 'react-native-dimension';

class HomeListItem extends Component{
    render() {
        return(
            <View style={{'flex' : 1, backgroundColor: 'blue'}}>
                <Image
                    source = {{uri: this.props.item.image}}
                    // resizeMode='contain'
                    style = {styles.listImageItem}
                    >
                </Image>
                <Text style={styles.listTitleItem}>{this.props.item.name}</Text>
                <Text style={styles.listDescriptionItem}>{this.props.item.description}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    listTitleItem :{
        color: 'yellow',
        fontSize: 20,
    },
    listDescriptionItem :{
        color: 'yellow',
        fontSize: 15,
    },
    listImageItem :{
        width: width(94),
        height:height(25),
    }
});
export default HomeListItem;