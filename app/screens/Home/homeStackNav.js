import { StackNavigator } from 'react-navigation';

import HomeScreen from './homeScreen';
import SelectScreen from './selectScreen';
import ConnectScreen from './connectScreen';
import VideoDetailScreen from '../Videos/videoDetailScreen';
import {transitionConfig} from '../../const/constData';

export default HomeStackNav = StackNavigator({
    HOME_MAIN: {
        screen: HomeScreen,
    },
    SELECT_FILES: {
        screen: SelectScreen,
        navigationOptions: {
            tabBarVisible: false,
        },
    },
    CONNECT: {
        screen: ConnectScreen,
        navigationOptions: {
            tabBarVisible: false,
        },
    },
    VIDEO_DETAIL: {
        screen: VideoDetailScreen,
        navigationOptions: {
            tabBarVisible: false,
        },
    },
}, {
    headerMode: 'none',
    transitionConfig,
});
