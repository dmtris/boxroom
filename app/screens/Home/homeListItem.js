import React, { Component } from 'react';
import { View, Image, StyleSheet, Text, TouchableOpacity } from 'react-native';
import ActiveToggle from '../Common/activeToggle';

import images from '../../const/images';

export default class HomeListItem extends Component{
    render() {
        return(
            <View style={styles.container}>
                <TouchableOpacity activeOpacity={0.8} onPress={this.props.onPress}>
                    <Image
                        source={this.props.item.image}
                        style={styles.listImageItem}
                    />
                </TouchableOpacity>

                <View style={styles.content}>
                    <View style={styles.descriptionContent}>
                        <Text style={styles.listTitleItem}>
                            {this.props.item.name}
                        </Text>
                        <Text style={styles.listDescriptionItem}>
                            {this.props.item.view_count} views  -  {this.props.item.day} days ago
                        </Text>
                    </View>

                    <View style={styles.buttonContent}>
                        <ActiveToggle active={true} image_active={images.icon_detail_like_active} image_deactive={images.icon_detail_like_deactive} description='1.1K' />
                        <ActiveToggle active={false} image_active={images.icon_detail_dislike_active} image_deactive={images.icon_detail_dislike_deactive} description='1.1K' />
                        <ActiveToggle image_deactive={images.icon_share_deactive} description='Share' />
                        <ActiveToggle image_deactive={images.icon_detail_menu} />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        // shadowColor: '#000',
        // shadowOpacity: 0.8,
        // shadowRadius: 2,
        // shadowOffset: {width: 1, height: 1,},
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
        borderWidth: 2,
        borderColor: '#ddd',
    },
    content: {
        width: '100%',
        height: 50, 
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    descriptionContent: {
        justifyContent: 'center',
        height: '100%',
        marginLeft: 10,
    },
    listImageItem :{
        width: '100%',
        height: 185,
    },
    listTitleItem :{
        fontSize: 12,
        color: '#333333',
        // borderWidth : 1,
    },
    listDescriptionItem :{
        fontSize: 8,
        color: '#333333',
        marginTop: 5, 
        // borderWidth : 1,
    },
    buttonContent: {
        flexDirection:'row', 
        alignItems: 'center',
    },
});