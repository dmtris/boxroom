import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Image, Animated } from 'react-native';
import {width, height} from 'react-native-dimension';
import NavBar from '../Common/navBar';
import MyButton from '../Common/myButton';
import images from '../../const/images';

class RippleAnim extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }


    render() {
        return (    
            <Animated.View 
                style={{
                    position: 'absolute',
                    width: this.props.size,
                    height: this.props.size,
                    borderRadius: this.props.size / 2,
                    backgroundColor: this.props.color,
                    opacity: this.props.opacity,
                    transform: [
                        {
                            scale: this.props.scale,
                        }
                    ],
                }}
            />
        )
    }
}

class RippleAnimSet extends Component {
    constructor(props) {
        super(props);
        this.state = {
            animTimer: new Animated.Value(0),
        };
    }

    componentDidMount() {
        Animated.loop(
            Animated.timing(this.state.animTimer, {
                toValue: this.props.duration,
                duration: this.props.duration,
            }),
        ).start();
    }

    render() {
        return (
            new Array(this.props.count).fill(null).map((item, idx) => {
                let timer = Animated.add(this.state.animTimer, Animated.multiply(new Animated.Value(400), new Animated.Value(-idx)));
                return (
                    <RippleAnim 
                        index={idx}
                        size={this.props.size}
                        color='white'
                        opacity={timer.interpolate({
                            inputRange: [-this.props.duration, -101, -100, 2000, 2001, this.props.duration],
                            outputRange: [0, 0, 0.5, 0, 0, 0],
                            // extrapolate: 'clamp',
                        })}
                        scale={timer.interpolate({
                            inputRange: [-this.props.duration, -101, -100, 2000, 2001, this.props.duration],
                            outputRange: [0, 0, 0, 1, 0, 0],
                            // extrapolate: 'clamp',
                        })}
                    />
                )
            })
        )
    }
}

export default class ConnectScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            animated: true,
            retry: false,
        };
        this.onStartRippleAnim();
    }

    onStartRippleAnim() {
        this.setState({
            animated: true,
            retry: false,
        });

        setTimeout(() => {
            this.setState({
                animated: false,
                retry: true,
            });
        }, 15000);
    }

    render() {
        return (
            <View style={styles.container}>
                <NavBar
                    title=''
                    onBackPress={() => {this.props.navigation.goBack()}}
                    onLogoPress={() => {}}
                />

                {
                    this.state.animated ? (
                        <View style={styles.animContainer}>
                            <RippleAnimSet
                                size={width(100)}
                                duration={5000}
                                count={8}
                            />

                            <View style={styles.retryButtonContainer}>
                                <Image
                                    source={images.image_connect_wifi}
                                    style={{
                                        resizeMode: 'contain', 
                                        width: 60,
                                        height: 60,
                                    }}
                                />
                            </View>

                            <View style={styles.phoneImageContent}>
                                <Image
                                    source={images.image_connect_phone}
                                    style={{
                                        resizeMode: 'contain', 
                                        width: 60,
                                        height: 60,
                                    }}
                                />
                            </View>
                        </View>
                    ) : null
                }

                {
                    this.state.retry ? (
                        <View style={styles.animContainer}>
                            <View style={{
                                position: 'absolute', 
                                width: '100%', 
                                height: 200, 
                                justifyContent: 'flex-end',
                                alignItems: 'center',
                            }}>
                                <Text style={{fontSize: 20, color: 'white'}}>
                                    Retry
                                </Text>
                            </View>

                            <TouchableOpacity 
                                onPress={this.onStartRippleAnim.bind(this)}
                                style={styles.retryButtonContainer}
                            >
                                <Image
                                    source={images.image_connect_retry}
                                    style={{
                                        resizeMode: 'contain', 
                                        width: 60,
                                        height: 60,
                                    }}
                                />
                            </TouchableOpacity>
                        </View>
                    ) : null
                }

                <View style={styles.buttonContainer}>
                    <View style={styles.buttonContent}>
                        <MyButton 
                            width='47%'
                            borderColor='white'
                            fillColor='#00bcd4'
                            textColor='white'
                            text='Connect to iOS'
                        />

                        <MyButton 
                            width='47%'
                            borderColor='white'
                            fillColor='#00bcd4'
                            textColor='white'
                            text='Connect to PC'
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#00bcd4',
    },
    buttonContainer: {
        width: '100%',
        height: 70,
        justifyContent: 'center',
        alignItems: 'center',
        // borderWidth: 1,
    },
    animContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonContent: {
        width: '90%',
        height: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        // borderWidth: 1,
    },
    retryButtonContainer: {
        position: 'absolute',
        width: 100,
        height: 100,
        borderRadius: 100 / 2,
        shadowColor: '#000',
        shadowOpacity: 0.4,
        shadowRadius: 2,
        shadowOffset: {width: 1, height: 1,},
        backgroundColor: '#005964',
        justifyContent: 'center',
        alignItems: 'center',
    },
    phoneImageContent: {
        position: 'absolute', 
        width: width(100),
        height: width(100),
        top: width(30),
        left: width(15),
    },
})