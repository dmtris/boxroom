import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, FlatList, Text, Image, TouchableOpacity } from 'react-native';

import NavBar from '../Common/navBar';
import SectionHeader from '../Common/sectionHeader';
import HomeListItem from './homeListItem';

import images from '../../const/images';
import constData from '../../const/constData';

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);

    console.disableYellowBox = true;

    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <NavBar
          title='Home'
          onHamburgerPress={() => {}}
          onLogoPress={() => {}}
        />
        <ScrollView style={styles.content}>
          <View>
            <SectionHeader title='Share Local Files' />

            <View style={styles.shareContent}>
              <TouchableOpacity style={styles.shareItem} onPress={() => this.props.navigation.navigate('SELECT_FILES')}>
                <Image
                  source={images.icon_share_send}
                  style={styles.shareItemImage}
                />
                <Text style={styles.shareItemText}>
                  Send
                </Text>
              </TouchableOpacity>

              <TouchableOpacity style={styles.shareItem}>
                <Image
                  source={images.icon_share_receive}
                  style={styles.shareItemImage}
                />
                <Text style={styles.shareItemText}>
                  Receive
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View>
            <SectionHeader title='Recommended' description='Based on your interest' />

            <FlatList
              data={constData.detailVideoList}
              renderItem={({item}) => {
                return (<HomeListItem item={item} onPress={() => this.props.navigation.navigate('VIDEO_DETAIL', item)}/>)
              }}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  content: {
    flex: 1,
  },
  shareContent: {
    width: '100%',
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  shareItem: {
    // width: '40%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  shareItemImage: {
    resizeMode: 'contain', 
    width: 45,
    height: 24,
  },
  shareItemText: {
    fontSize: 16,
    color: '#333333',
  },
})
