import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';
import NavBar from '../Common/navBar';
import DetailFileView from '../Common/detailFileView';
import DetailListView from '../Common/detailListView';
import DetailGridView from '../Common/detailGridView';
import CategoryBar from '../Common/categoryBar';
import MyButton from '../Common/myButton';
import constData from '../../const/constData';

export default class SelectScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
        select_id: 0
    };
  }

  componentWillMount() {
      constData.categoryList.map((item, index) => {
          item.data.map((listItem, list_index) => {
              listItem.index = list_index;
              listItem.selected = false;
          });
      });
  }

  onSelectCategory(id) {
      this.setState({
          select_id: id
      });
  }

  onSelectItem(index) {
    constData.categoryList[this.state.select_id].data[index].selected = !constData.categoryList[this.state.select_id].data[index].selected;
  }

  render() {
    // alert(JSON.stringify(this.props.navigation.state.params.data));
    return (
      <View style={styles.container}>
        <NavBar
          title='Select Files'
          onBackPress={() => {this.props.navigation.goBack()}}
          onSearchPress={() => {}}
          onLogoPress={() => {}}
        />

        <View style={{flex:1}}>
            <CategoryBar 
                select_id={this.state.select_id} 
                data={constData.categoryList} 
                onSelectCategory={this.onSelectCategory.bind(this)}
            />

            {
                constData.categoryList[this.state.select_id].screen === 'fileView' ?
                    <DetailFileView />
                    : null
            }
            
            {
                constData.categoryList[this.state.select_id].screen === 'listView' ?
                    <DetailListView 
                        data={constData.categoryList[this.state.select_id].data}
                        selectAvailable={true}
                        onSelectItem={this.onSelectItem.bind(this)}
                    /> : null
            }

            {
                constData.categoryList[this.state.select_id].screen === 'gridView' ?
                    <DetailGridView 
                        data={constData.categoryList[this.state.select_id].data}
                        selectAvailable={true}
                        onSelectItem={this.onSelectItem.bind(this)}
                    /> : null
            }
        </View>

        <View style={styles.buttonContainer}>
            <MyButton 
                borderColor='#00bcd4'
                fillColor='white'
                textColor='#00bcd4'
                text='SELECTED (6)'
            />

            <MyButton 
                borderColor='#00bcd4'
                fillColor='#00bcd4'
                textColor='white'
                text='NEXT'
                onPress={() => this.props.navigation.navigate('CONNECT')}
            />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    buttonContainer: {
        width: '100%',
        height: 55,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        // borderWidth: 1,
    },
})