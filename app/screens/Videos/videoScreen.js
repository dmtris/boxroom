import React, { Component } from 'react';
import { View, StyleSheet, } from 'react-native';
import NavBar from '../Common/navBar';
import DetailListView from '../Common/detailListView';
import constData from '../../const/constData';

export default class VideoScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  navigateToDetailPage(item) {
    this.props.navigation.navigate('VIDEO_DETAIL', item)
  }

  render() {
    return (
      <View style={styles.container}>
        <NavBar
          title='Videos'
          onHamburgerPress={() => {}}
          onSearchPress={() => {}}
          onLogoPress={() => {}}
        />

        <DetailListView data={constData.detailVideoList} onSelectItem={this.navigateToDetailPage.bind(this)} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
})