import { StackNavigator } from 'react-navigation';

import VideoScreen from './videoScreen';
import VideoDetailScreen from './videoDetailScreen';
import {transitionConfig} from '../../const/constData';

export default VideoStackNav = StackNavigator({
    VIDEO_HOME: {
        screen: VideoScreen
    },
    VIDEO_DETAIL: {
        screen: VideoDetailScreen,
        navigationOptions: {
            tabBarVisible: false,
        },
    },
}, {
    headerMode: 'none',
    transitionConfig,
});
