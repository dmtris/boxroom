import { StackNavigator } from 'react-navigation';

import PictureScreen from './pictureScreen';
import PictureDetailScreen from './pictureDetailScreen';
import {transitionConfig} from '../../const/constData';

export default PictureStackNav = StackNavigator({
    PICTURE_MAIN: {
        screen: PictureScreen,
    },
    PICTURE_DETAIL: {
        screen: PictureDetailScreen,
        navigationOptions: {
            tabBarVisible: false,
        },
    },
}, {
    headerMode: 'none',
    transitionConfig,
});
