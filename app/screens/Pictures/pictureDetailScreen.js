import React, { Component } from 'react';
import { Platform, View, StyleSheet, StatusBar,Text, Image, TouchableOpacity } from 'react-native';
import NavBar from '../Common/navBar';
import {width,height} from 'react-native-dimension';
import images from '../../const/images';
import ActiveToggle from '../Common/activeToggle';

export default class PictureDetailScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    // alert(JSON.stringify(this.props.navigation.state.params));
    return (
      <View style={styles.container}>
        {
            <StatusBar hidden={Platform.select({ios: true, android: false})}/>
        }
        
        <Image
            source={this.props.navigation.state.params.image}
            style={{
                position: 'absolute',
                resizeMode: 'contain', 
                width: '100%',
                height: '100%',
                // height: this.props.navigation.state.params.type === 'gif' ? width(100) : width(100) * 16 / 9,
            }}
        />

        <NavBar
          title={this.props.navigation.state.params.name}
          onBackPress={() => {this.props.navigation.goBack()}}
          onMorePress={() => {}}
          style={{backgroundColor: '#00000080', paddingTop: 0}}
        />

        <View style={styles.buttonContent}>
            <ActiveToggle active={false} image_active={images.icon_detail_like_active} image_deactive={images.icon_picture_like_deactive} description='1.1K' 
                container_style={styles.toggleContainerStyle}
                image_style={styles.toggleImageStyle}
                description_style={styles.toggleDescriptionStyle}
            />
            <ActiveToggle active={false} image_active={images.icon_detail_dislike_active} image_deactive={images.icon_picture_dislike_deactive} description='1.1K' 
                container_style={styles.toggleContainerStyle}
                image_style={styles.toggleImageStyle}
                description_style={styles.toggleDescriptionStyle}
            />
            <ActiveToggle image_deactive={images.icon_picture_share_deactive} description='Share' 
                container_style={styles.toggleContainerStyle}
                image_style={styles.toggleImageStyle}
                description_style={styles.toggleDescriptionStyle}
            />
            <ActiveToggle image_deactive={images.icon_picture_download_deactive} description='Download' 
                container_style={styles.toggleContainerStyle}
                image_style={styles.toggleImageStyle}
                description_style={styles.toggleDescriptionStyle}
            />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    justifyContent: 'space-between',
  },
  buttonContent: {
      backgroundColor: '#00000080',
      height: 55,
      paddingLeft: width(5),
      paddingRight: width(5),
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center',
  },
  toggleContainerStyle: {
      width: 80,
  },
  toggleImageStyle: {
      height: 20,
      marginBottom: 5,
  },
  toggleDescriptionStyle: {
      fontSize: 11,
  },
})
