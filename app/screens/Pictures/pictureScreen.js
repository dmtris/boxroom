import React, { Component } from 'react';
import { View, StyleSheet, FlatList, Text, Image, TouchableOpacity } from 'react-native';
import NavBar from '../Common/navBar';
import SectionHeader from '../Common/sectionHeader';
import images from '../../const/images';
import constData from '../../const/constData';
import {width,height} from 'react-native-dimension';

class PictureCategoryItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    // alert(JSON.stringify(this.props.item.data));
    return (
      <View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
          <SectionHeader title={this.props.info.title} />

          <TouchableOpacity style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
            <Text style={{fontSize: 10, color: '#666666'}}>
              View More
            </Text>

            <Image
              source={images.icon_detail_arrow_right}
              style={styles.viewMoreButtonImage}
            />
          </TouchableOpacity>
        </View>

        <FlatList
          style={styles.categoryItemData}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          data={this.props.info.data}
          renderItem={ ({item}) => (
            <TouchableOpacity
              activeOpacity={0.8}
              style={{
                marginLeft: width(2),
                width: width(30),
                height: this.props.info.type === 'gif' ? width(30) : (width(30) * 16 / 9),
                // borderWidth: 1,
              }}
              onPress={() => this.props.onPress(item)}
            >
              <Image
                source={item.image}
                style={{width: '100%', height: '100%'}}
              />
            </TouchableOpacity>
          )}
        />
      </View>
    )
  }
}

export default class PictureScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };

    this.navigateToDetailPage = this.navigateToDetailPage.bind(this);
  }

  navigateToDetailPage(item) {
    this.props.navigation.navigate('PICTURE_DETAIL', item)
  }

  render() {
    return (
      <View style={styles.container}>
        <NavBar
          title='Pictures'
          onHamburgerPress={() => {}}
          onSearchPress={() => {}}
          onDownloadPress={() => {}}
          onLogoPress={() => {}}
        />

        <FlatList
          style={styles.content}
          data={constData.pictureList}
          renderItem={ ({item}) => (
            <PictureCategoryItem info={item} onPress={this.navigateToDetailPage}/>
          )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  content: {
    flex: 1,
  },
  viewMoreButtonImage: {
    resizeMode: 'contain', 
    width: 20, 
    height: 12,
    marginRight: 10,
  },
  categoryItemData: {
    marginLeft: width(1),
    marginRight: width(1),
    // width: '100%',
  }
})
