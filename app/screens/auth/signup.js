import React, { Component } from 'react';
import { View, Image, StyleSheet, Text, TextInput, Platform, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {width,height} from 'react-native-dimension';
import images from '../../const/images';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

class SignUpScreen extends Component{
    constructor(props){
        super(props);
        this.state = {
            firstActive: false,
            lastActive: false,
            passwordActive: false,
            emailActive: false,
            phoneActive: false,
        };
    }

    render(){

        return(
            <View
                style={styles.inputContainer}>

                <KeyboardAwareScrollView  
                    resetScrollToCoords={{x : 0, y: 0}}
                    scrollEnabled={false}
                    behavior="padding" 
                    enabled>
                    <Text
                        style={styles.titleText}>
                        C&nbsp;R&nbsp;E&nbsp;A&nbsp;T&nbsp;E&nbsp;&nbsp;N&nbsp;E&nbsp;W&nbsp;&nbsp;A&nbsp;C&nbsp;C&nbsp;O&nbsp;U&nbsp;N&nbsp;T
                    </Text>

                    <View style={[styles.inputSection,{borderColor: this.state.firstActive === true ? '#00bcd4' : '#cccccc'}]}>
                        <Icon style={[styles.userIcon,{color: this.state.firstActive === true ? '#00bcd4' : '#999999'}]} name="user" size={20}/>
                        <TextInput 
                            style={styles.inputBox}
                            underlineColorAndroid='rgba(0,0,0,0)' 
                            placeholder="Firstname"
                            placeholderTextColor={this.state.firstActive === true ? '#00bcd4' : '#999999'}
                            onFocus={() => this.setState({firstActive:true})}
                            onBlur={() => this.setState({firstActive:false})}
                            />
                    </View>

                    <View style={[styles.inputSection,{marginTop:height(3)}, {borderColor: this.state.lastActive === true ? '#00bcd4' : '#cccccc'}]}>
                        <Icon style={[styles.userIcon,{color: this.state.lastActive === true ? '#00bcd4' : '#999999'}]} name="user" size={20}/>
                        <TextInput 
                            style={[styles.inputBox]}
                            underlineColorAndroid='rgba(0,0,0,0)' 
                            placeholder="Lastname"
                            placeholderTextColor={this.state.lastActive === true ? '#00bcd4' : '#999999'}
                            onFocus={() => this.setState({lastActive:true})}
                            onBlur={() => this.setState({lastActive:false})}
                            />
                    </View>

                    <View style={[styles.inputSection,{marginTop:height(3)}, {borderColor: this.state.emailActive === true ? '#00bcd4' : '#cccccc'}]}>
                        <Icon style={[styles.userIcon,{color: this.state.emailActive === true ? '#00bcd4' : '#999999'}]} name="envelope" size={20}/>
                        <TextInput 
                            style={[styles.inputBox]}
                            underlineColorAndroid='rgba(0,0,0,0)' 
                            placeholder="Email"
                            autoCapitalize='none'
                            placeholderTextColor={this.state.emailActive === true ? '#00bcd4' : '#999999'}
                            onFocus={() => this.setState({emailActive:true})}
                            onBlur={() => this.setState({emailActive:false})}
                            />
                    </View>

                    <View style={[styles.inputSection,{marginTop:height(3)}, {borderColor: this.state.phoneActive === true ? '#00bcd4' : '#cccccc'}]}>
                        <Icon style={[styles.userIcon,{color: this.state.phoneActive === true ? '#00bcd4' : '#999999'}]} name="phone" size={20}/>
                        <TextInput 
                            style={[styles.inputBox]}
                            underlineColorAndroid='rgba(0,0,0,0)' 
                            placeholder="Phone"
                            autoCapitalize='none'
                            placeholderTextColor={this.state.phoneActive === true ? '#00bcd4' : '#999999'}
                            onFocus={() => this.setState({phoneActive:true})}
                            onBlur={() => this.setState({phoneActive:false})}
                            />
                    </View>

                    <View style={[styles.inputSection,{marginTop:height(3)}, {borderColor: this.state.passwordActive === true ? '#00bcd4' : '#cccccc'}]}>
                        <Icon style={[styles.userIcon,{color: this.state.passwordActive === true ? '#00bcd4' : '#999999'}]} name="lock" size={20}/>
                        <TextInput 
                            style={[styles.inputBox]}
                            underlineColorAndroid='rgba(0,0,0,0)' 
                            placeholder="Password"
                            autoCapitalize='none'
                            secureTextEntry={true}
                            placeholderTextColor={this.state.passwordActive === true ? '#00bcd4' : '#999999'}
                            onFocus={() => this.setState({passwordActive:true})}
                            onBlur={() => this.setState({passwordActive:false})}
                            />
                    </View>


                    <TouchableOpacity>
                        <Image
                            source={images.signup_but}
                            resizeMode='contain'
                            style={styles.button}
                        />
                    </TouchableOpacity>
                    
                    <View style={{flexDirection: 'row', justifyContent:'center'}}>
                        <Text
                            style={[styles.normalText,{marginTop: height(2)}]}>
                            Already Have An Account? 
                        </Text>
                        <TouchableOpacity>
                            <Text
                                style={[styles.normalText,{marginTop: height(2),fontWeight: 'bold',color:'#00bcd4'}]}>
                                &nbsp;LOGIN
                            </Text>
                        </TouchableOpacity>
                    </View>
        
                </KeyboardAwareScrollView>

                <View 
                    style={{paddingBottom:10, height:height(7.5)}}>
                    <View style={{flexDirection: 'row', justifyContent:'center'}}>
                        <Text
                            style={[styles.normalText]}>
                            By signing in you agree to Boxroom`s 
                        </Text>
                        <TouchableOpacity>
                            <Text
                                style={[styles.normalText,{fontWeight: 'bold'}]}>
                                &nbsp;Terms of Services
                            </Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{flexDirection: 'row', justifyContent:'center'}}>
                        <Text
                            style={[styles.normalText,{marginTop: height(0.5)}]}>
                            and 
                        </Text>
                        <TouchableOpacity>
                            <Text
                                style={[styles.normalText,{marginTop: height(0.5), fontWeight: 'bold'}]}>
                                &nbsp;Privacy Policy
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
        
    inputContainer: {
        flex: 1,
        paddingTop: height(5),
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: 'green'
    },

    inputSection: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#fff',
        borderRadius: 10,
        borderWidth: 1,
        // borderColor: 'rgba(0,255,0,1)',
        borderColor: '#cccccc',
        backgroundColor: '#fbfbfb',
    },

    userIcon: {
        padding: 10,
    },

    inputBox: {
        flex: 1,
        width: width(85),
        ...Platform.select({
            ios: {
              height: height(7),
              fontSize: 14,
            },
            android: {
                fontSize: 14,
            },
        }),
        // backgroundColor: 'rgba(255,0,0,1)',
        
        paddingHorizontal: 16,
        color: '#999999',
        // marginVertical: height(1.5),
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 0,
    
    },

    button: {
        width: width(85),
        height: height(10),
        // ...Platform.select({
        //     ios: {
        //       marginTop: height(5),
        //     },
        //     android: {
        //         marginTop: height(2.5),
        //     },
        // }),
        marginTop: height(4),
    },

    titleText: {
        // marginVertical: height(2),
        ...Platform.select({
            ios: {
              fontSize: 17,
              marginVertical: height(5),
            },
            android: {
                fontSize: 20,
                marginBottom: height(5),
            },
        }),
        // color: 'rgba(0,0,0,1)',
        
        color: '#00bcd4',
        alignSelf: 'center',
        fontWeight: 'bold',
    },

    normalText: {
        // marginVertical: height(2),
        ...Platform.select({
            ios: {
              fontSize: 12,
            },
            android: {
                fontSize: 14,
            },
        }),
        // color: 'rgba(0,0,0,1)',
        color: '#999999',
        alignSelf: 'center',
    }
});
export default SignUpScreen;