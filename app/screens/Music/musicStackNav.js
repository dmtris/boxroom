import { StackNavigator } from 'react-navigation';

import MusicScreen from './musicScreen';
import {transitionConfig} from '../../const/constData';

export default MusicStackNav = StackNavigator({
    MUSIC_HOME: {
        screen: MusicScreen
    },
}, {
    headerMode: 'none',
    navigationOptions: {
        headerVisible: false,
        transitionConfig,
    }
});
