import React, { Component } from 'react';
import { View, StyleSheet, } from 'react-native';
import NavBar from '../Common/navBar';
import DetailListView from '../Common/detailListView';
import constData from '../../const/constData';

export default class MusicScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <NavBar
          title='Music'
          onHamburgerPress={() => {}}
          onSearchPress={() => {}}
          onLogoPress={() => {}}
        />

        <DetailListView data={constData.detailMusicList} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
})