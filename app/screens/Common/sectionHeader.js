import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default class SectionHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={styles.sectionHeader}>
                <View style={styles.sectionHeaderMark} />

                <View style={styles.sectionHeaderTitle} >
                    <Text style={styles.sectionHeaderTitleText}>
                        {this.props.title}
                    </Text>
                </View>

                {
                    this.props.description !== undefined ?
                        <View style={styles.sectionHeaderTitle} >
                            <Text style={styles.sectionHeaderDescriptionText}>
                                - {this.props.description}
                            </Text>
                        </View>
                        : null
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    sectionHeader: {
        // width: '100%',
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        // borderWidth: 1,
    },
    sectionHeaderMark: {
        width: 4,
        height: 18,
        marginLeft: 10,
        backgroundColor: '#00bcd4',
        // borderWidth: 1,
    },
    sectionHeaderTitle : {
        height: 20,
        marginLeft: 10,
        justifyContent: 'center',
        // borderWidth: 1,
    },
    sectionHeaderTitleText : {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#00bcd4',
        // borderWidth: 1,
    },    
    sectionHeaderDescriptionText : {
        fontSize: 12,
        color: '#666666',
        // borderWidth: 1,
    },    
})
