import React, { Component } from 'react';
import { View, Text, StyleSheet, Platform, TouchableOpacity, Image } from 'react-native';
import {width,height} from 'react-native-dimension';
import images from '../../const/images';

export default class NavBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { title, onHamburgerPress, onBackPress, onSearchPress, onDownloadPress, onLogoPress, onMorePress } = this.props;

        return (
            <View style={[styles.outerContainer, this.props.style !== undefined ? this.props.style : null]}>
                <View style={styles.innerContainer}>
                    <View style={styles.subContainer}>
                        {
                            onHamburgerPress !== undefined ?
                                <TouchableOpacity onPress={onHamburgerPress}>
                                    <Image
                                        source={images.navtop_icon_hamburger}
                                        style={styles.navHamburger}
                                    />
                                </TouchableOpacity>
                                : null
                        }

                        {
                            onBackPress !== undefined ?
                                <TouchableOpacity onPress={onBackPress}>
                                    <Image
                                        source={images.navtop_icon_backbutton}
                                        style={styles.navHamburger}
                                    />
                                </TouchableOpacity>
                                : null
                        }
                        
                        <View style= {styles.navTitleBG}>
                            <Text 
                                style = {styles.navTitle}>
                                {title}
                            </Text>
                        </View>
                    </View>

                    <View style={styles.subContainer}>
                        {
                            onSearchPress !== undefined ?
                                <TouchableOpacity onPress={onSearchPress} style= {styles.navButBg} >
                                    <Image
                                        source={images.navtop_icon_search}
                                        style={styles.navBut}
                                    />
                                </TouchableOpacity>
                                : null
                        }

                        {
                            onDownloadPress !== undefined ?
                                <TouchableOpacity onPress={onDownloadPress} style= {styles.navButBg} >
                                    <Image
                                        source={images.navtop_icon_download}
                                        style={styles.navBut}
                                    />
                                </TouchableOpacity>
                                : null
                        }

                        {
                            onLogoPress !== undefined ?
                                <TouchableOpacity onPress={onLogoPress} style= {styles.navButBg} >
                                    <Image
                                        source={images.navtop_icon_logo}
                                        style={styles.navBut}
                                    />
                                </TouchableOpacity>
                                : null
                        }

                        {
                            onMorePress !== undefined ?
                                <TouchableOpacity onPress={onMorePress} style= {styles.navButBg} >
                                    <Image
                                        source={images.icon_detail_menu}
                                        style={styles.navBut}
                                    />
                                </TouchableOpacity>
                                : null
                        }
                    </View>
                    
                
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    outerContainer: {
        width: '100%',
        backgroundColor: '#00bcd4',
        paddingTop: Platform.select({ios: 20, android: 0}),
        alignItems: 'center',
    },
    innerContainer: {
        // backgroundColor: 'blue',
        width: '90%',
        height: 60,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    subContainer: {
        // backgroundColor: 'green',
        flexDirection:'row', 
        alignItems: 'center',
    },
    navHamburger:{
        resizeMode: 'contain', 
        width:width(5), 
    },
    navTitleBG :{
        marginLeft: width(5),
        alignContent: 'center',
        justifyContent: 'center',
    },

    navTitle :{
        fontWeight: 'bold',
        fontSize: 20,
        color: '#fff',
    },

    navButBg:{
        marginLeft: 10,
        width: 40,
        height: 25,
        justifyContent:'center',
        alignContent:'center',
        // borderWidth: 1,
    },

    navBut:{
        resizeMode: 'contain', 
        width: '100%',
        height: '100%',
    },

})
