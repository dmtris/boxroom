import React, { Component } from 'react';
import { View, FlatList, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import images from '../../const/images';

class DetailListItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: this.props.item.selected,
        };
    }

    render() {
        return (
            <TouchableOpacity 
                activeOpacity={0.8}
                style={styles.listItem} 
                onPress={() => {
                    if(this.props.selectAvailable) {
                        this.setState({
                            selected: !this.state.selected
                        });
                        this.props.onSelectItem !== undefined ? this.props.onSelectItem(this.props.index) : null;
                    } else {
                        this.props.onSelectItem !== undefined ? this.props.onSelectItem(this.props.item) : null;
                    }
                }}
            >
                <View style={styles.listContainer}>
                    <View style={{height: '100%', flexDirection: 'row'}}>
                        <View style={styles.listItemImageView}>
                            <Image
                                source={this.props.item.image}
                                style={styles.listItemImage}
                            />
                        </View>
                        <View style={styles.listItemDescriptionView}>
                            <View style={{height: 20, justifyContent:'center'}}>
                                <Text>
                                    {this.props.item.name}
                                </Text>
                            </View>
                            <View style={{height: 15, flexDirection: 'row', alignItems:'center'}}>
                                <View style={{width: '25%'}}>
                                    <Text style={{fontSize: 10, color: '#666666'}}>
                                        {this.props.item.size}
                                    </Text>
                                </View>
                                <View style={{width: '35%'}}>
                                    <Text style={{fontSize: 10, color: '#666666'}}>
                                        {this.props.item.date}
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </View>

                    {
                        this.props.selectAvailable ? (
                            <View style={styles.selectContent}>
                                <Image
                                    source={(this.props.item.selected) ? images.icon_select_active : images.icon_select_deactive}
                                    style={{width: 16, height: 16}}
                                />
                            </View>
                        ) : null
                    }
                </View>

                <View style={{height: 1, backgroundColor: '#a4a4a4'}}/>
            </TouchableOpacity>
        );
    }
}

export default class DetailListView extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    // alert(JSON.stringify(this.props.data));
    return (
        <View style={styles.content}>
            <FlatList
                data={this.props.data}
                renderItem={ ({item, index}) => (
                    <DetailListItem 
                        index={index}
                        item={item}
                        selectAvailable={this.props.selectAvailable !== undefined ? this.props.selectAvailable : false}
                        selected={item.selected !== undefined ? item.selected : false}
                        onSelectItem={this.props.onSelectItem}
                    />
                )}
            />
        </View>
    );
  }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    listItem: {
        width: '100%',
        height: 55,
        // borderWidth: 1,
    },
    listContainer: {
        height: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    listItemImageView: {
        width: 60,
        height: '100%', 
        justifyContent: 'center',
        alignItems: 'center',
        // borderWidth: 1,
    },
    listItemImage: {
        resizeMode: 'contain', 
        width: 40,
        height: 40,
    },
    listItemDescriptionView: {
      justifyContent: 'center',
    },
    listItemDescriptionSubView: {
        height: 20, 
        justifyContent: 'center',
        // borderWidth: 1,
    },
    selectContent: {
        width: 50, 
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        // borderWidth: 1,
    },
})