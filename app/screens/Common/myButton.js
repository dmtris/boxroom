import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

export default class MyButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
  
    render() {
        return (
            <TouchableOpacity 
                onPress={() => {(this.props.onPress !== undefined) ? this.props.onPress() : null }}
                activeOpacity={0.8} 
                style={{
                    backgroundColor: this.props.borderColor, 
                    width: (this.props.width != undefined) ? this.props.width : '48%',
                    height: (this.props.height != undefined) ? this.props.height : 40,
                }}
            >
                <View style={[styles.buttonInner, {backgroundColor: this.props.fillColor}]}>
                    <Text style={{color: this.props.textColor}}>
                        {this.props.text}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    buttonInner: {
        flex: 1,
        marginTop: 1,
        marginBottom: 1,
        marginLeft: 1,
        marginRight: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})
