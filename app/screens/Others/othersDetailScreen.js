import React, { Component } from 'react';
import { View, StyleSheet, } from 'react-native';
import NavBar from '../Common/navBar';
import DetailListView from '../Common/detailListView';

export default class OthersDetailScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <NavBar
          title={this.props.navigation.state.params.title}
          onBackPress={() => {this.props.navigation.goBack()}}
          onSearchPress={() => {}}
          onLogoPress={() => {}}
        />

        <DetailListView data={this.props.navigation.state.params.data} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
})