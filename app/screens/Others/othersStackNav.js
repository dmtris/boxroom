import { StackNavigator } from 'react-navigation';

import OthersScreen from './othersScreen';
import OthersDetailScreen from './othersDetailScreen';
import {transitionConfig} from '../../const/constData';

export default OthersStackNav = StackNavigator({
    OTHERS_HOME: {
        screen: OthersScreen
    },
    OTHERS_DETAIL: {
        screen: OthersDetailScreen
    }
}, {
    headerMode: 'none',
    transitionConfig,
});
