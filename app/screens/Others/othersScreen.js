import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import NavBar from '../Common/navBar';
import GridView from 'react-native-super-grid';
import CategoryItem from '../Common/categoryItem';
import { width } from 'react-native-dimension';
import constData from '../../const/constData';

export default class OthersScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <NavBar
          title='Others'
          onHamburgerPress={() => {}}
          onSearchPress={() => {}}
          onLogoPress={() => {}}
        />
        <View style={styles.content}>
          <GridView
            style={styles.gridView}
            // itemDimension={width(40)}
            spacing={width(5)}
            items={constData.detailListData}
            renderItem={ item  => (
              <CategoryItem 
                item={item} 
                onPress={() => this.props.navigation.navigate(item.screen, item)}
              />
            )}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  content: {
    flex: 1,
  },
  gridView: {
  },
  categoryItem: {
    width: '100%',
    height: 60,
    flexDirection: 'row',
    // borderWidth: 1,
  }, 
  categoryItemImageView: {
    width: 50,
    height: '100%', 
    justifyContent: 'center',
    alignItems: 'center',
    // borderWidth: 1,
  },
  categoryItemImage:{
    resizeMode: 'contain', 
    width: 15,
    height: 15,
  },
  categoryItemDescriptionView: {
    justifyContent: 'center',
  },
})