import React, { Component } from 'react';
import {  View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { NavigationActions } from 'react-navigation';
import images from '../../const/images';

export default class HomeMenuScreen extends Component {
    navigateToScreen = (route) => () => {
        const navigationAction = NavigationActions.navigate({
            routeName: route
        });
        this.props.navigation.dispatch(navigationAction);
    }

    logout = () => () => {
        alert('logout')
    }

    render() {
        const { index } = this.props.navigation.state;
        const isHome = index === 0;
        const isTrending = index === 1;
        const isSubscription = index === 2;
        const isHistory = index === 3;
        const isLiked = index === 4;

        return (
            <View style={styles.container}>
                <View style={styles.userInfoContainer}>
                    <View style={styles.photoView}>
                        <Image source={images.photo} style={styles.photo} resizeMode='contain'/>
                    </View>
                    <Text style={styles.email}>UserEmailId@gmail.com</Text>
                </View>
                <View style={styles.buttonListContainer}>
                    <TouchableOpacity
                        style={styles.menuButton}
                        onPress={this.navigateToScreen('Home')}
                    >
                        <Image source={isHome ? images.home_active : images.home_inactive} style={styles.menuButtonIcon} resizeMode='contain' />
                        <Text style={[styles.menuButtonText, {color: isHome ? '#00bcd4' : '#aaa'}]}>Home</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.menuButton}
                        onPress={this.navigateToScreen('Trend')}
                    >
                        <Image source={isTrending ? images.trending_active : images.trending_inactive} style={styles.menuButtonIcon} resizeMode='contain' />
                        <Text style={[styles.menuButtonText, {color: isTrending ? '#00bcd4' : '#aaa'}]}>Trending</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.menuButton}
                        onPress={this.navigateToScreen('Subscription')}
                    >
                        <Image source={isSubscription ? images.subscription_active : images.subscription_inactive} style={styles.menuButtonIcon} resizeMode='contain' />
                        <Text style={[styles.menuButtonText, {color: isSubscription ? '#00bcd4' : '#aaa'}]}>Subscription</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.menuButton}
                        onPress={this.navigateToScreen('History')}
                    >
                        <Image source={isHistory ? images.history_active : images.history_inactive} style={styles.menuButtonIcon} resizeMode='contain' />
                        <Text style={[styles.menuButtonText, {color: isHistory ? '#00bcd4' : '#aaa'}]}>History</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.menuButton}
                        onPress={this.navigateToScreen('Liked')}
                    >
                        <Image source={isLiked ? images.liked_active : images.liked_inactive} style={styles.menuButtonIcon} resizeMode='contain' />
                        <Text style={[styles.menuButtonText, {color: isLiked ? '#00bcd4' : '#aaa'}]}>Liked</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity
                    style={[styles.menuButton, {marginTop: 10}]}
                    onPress={this.logout()}
                >
                    <Image source={images.logout} style={styles.menuButtonIcon} resizeMode='contain' />
                    <Text style={[styles.menuButtonText, {color: '#aaa'}]}>Logout</Text>
                </TouchableOpacity>
            </View>
        );
      }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    userInfoContainer: {
        width: '100%',
        height: 190,
        backgroundColor: '#00bcd4',
        alignItems: 'center',
        paddingTop: 50,
    },
    photoView: {
        width: 70,
        height: 70,
        borderRadius: 35,
        overflow: 'hidden',
        marginBottom: 25,
    },
    photo: {
        width: 70,
        height: 70,
        backgroundColor: 'white'
    },
    email: {
        fontSize: 16,
        color: 'white'
    },
    buttonListContainer: {
        width: '100%',
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
    },
    menuButton: {
        width: '100%',
        height: 53,
        flexDirection: 'row',
        alignItems: 'center',
    },
    menuButtonIcon: {
        width: 22,
        height: 25,
        marginLeft: 20,
        marginRight: 30,
    },
    menuButtonText: {
        fontSize: 15,
    }
});
