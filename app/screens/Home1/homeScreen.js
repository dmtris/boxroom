import React, { Component } from 'react';
import { StyleSheet, FlatList, View, Text, Image, TouchableOpacity} from 'react-native';
import { DrawerNavigator, StackNavigator } from 'react-navigation';
import {width,height} from 'react-native-dimension';

import images from '../../const/images';
import homeListData from '../../const/homeListData';
import HomeMenuScreen from './homeMenu';
import HomeListItem from '../../components/HomeListItem';

class HomeScreen extends Component {
    constructor(props) {
        super(props);
        console.disableYellowBox = true;

    }
    render()
    {
        return(
            <View style = { styles.MainContainer }>
                <TouchableOpacity>
                    <Image
                        source={images.recommend_but}
                        resizeMode='contain'
                        style={styles.modalButton}
                    />
                </TouchableOpacity>
                
                <FlatList
                    style = {{paddingTop: height(2)}}
                    data={homeListData}
                    refreshing= {true}
                    renderItem={({item,index}) => {
                        return (<HomeListItem item = {item} index = {index}></HomeListItem>)
                    }}
                />
            </View>
        );
    }
}

class TrendScreen extends Component {
    constructor(props) {
        super(props);
        console.disableYellowBox = true;

    }
    render()
    {
        return(
            <View style = { styles.MainContainer }>

                <Text
                    style={{fontSize: 23}}
                    >
                    Trend Screen View
                </Text>
            </View>
        );
    }
}

class SubscriptionScreen extends Component {
    constructor(props) {
        super(props);
        console.disableYellowBox = true;

    }
    render()
    {
        return(
            <View style = { styles.MainContainer }>

                <Text
                    style={{fontSize: 23}}
                    >
                    Subscription Screen View
                </Text>
            </View>
        );
    }
}

class HistoryScreen extends Component {
    constructor(props) {
        super(props);
        console.disableYellowBox = true;

    }
    render()
    {
        return(
            <View style = { styles.MainContainer }>

                <Text
                    style={{fontSize: 23}}
                    >
                    History Screen View
                </Text>
            </View>
        );
    }
}

class LikedScreen extends Component {
    constructor(props) {
        super(props);
        console.disableYellowBox = true;

    }
    render()
    {
        return(
            <View style = { styles.MainContainer }>

                <Text
                    style={{fontSize: 23}}
                    >
                    Liked Screen View
                </Text>
            </View>
        );
    }
}

class LogoutScreen extends Component {
    constructor(props) {
        super(props);
        console.disableYellowBox = true;

    }
    render()
    {
        return(
            <View style = { styles.MainContainer }>

                <Text
                    style={{fontSize: 23}}
                    >
                    Logout Screen View
                </Text>
            </View>
        );
    }
}

const HomeScreen_StackNavigator = StackNavigator({
    First: { 
        screen: HomeScreen, 
        navigationOptions: ({ navigation }) => ({
        // title: 'MainActivity111111',
        headerLeft : <NavDrawer navigationProps={ navigation }/>,

        headerStyle: {
            backgroundColor: '#00bcd4'
        },
        headerTintColor: '#fff',
        })
    },
});

const TrendScreen_StackNavigator = StackNavigator({
    First: { 
        screen: TrendScreen, 
        navigationOptions: ({ navigation }) => ({
        // title: 'MainActivity111111',
        headerLeft : <NavDrawer navigationProps={ navigation }/>,

        headerStyle: {
            backgroundColor: '#00bcd4'
        },
        headerTintColor: '#fff',
        })
    },
});

const SubscriptionScreen_StackNavigator = StackNavigator({
    First: { 
        screen: SubscriptionScreen, 
        navigationOptions: ({ navigation }) => ({
        // title: 'MainActivity111111',
        headerLeft : <NavDrawer navigationProps={ navigation }/>,

        headerStyle: {
            backgroundColor: '#00bcd4'
        },
        headerTintColor: '#fff',
        })
    },
});

const HistoryScreen_StackNavigator = StackNavigator({
    First: { 
        screen: HistoryScreen, 
        navigationOptions: ({ navigation }) => ({
        // title: 'MainActivity111111',
        headerLeft : <NavDrawer navigationProps={ navigation }/>,

        headerStyle: {
            backgroundColor: '#00bcd4'
        },
        headerTintColor: '#fff',
        })
    },
});

const LikedScreen_StackNavigator = StackNavigator({
    First: { 
        screen: LikedScreen, 
        navigationOptions: ({ navigation }) => ({
        // title: 'MainActivity111111',
        headerLeft : <NavDrawer navigationProps={ navigation }/>,

        headerStyle: {
            backgroundColor: '#00bcd4'
        },
        headerTintColor: '#fff',
        })
    },
});

const LogoutScreen_StackNavigator = StackNavigator({
    First: { 
        screen: LogoutScreen, 
        navigationOptions: ({ navigation }) => ({
        // title: 'MainActivity111111',
        headerLeft : <NavDrawer navigationProps={ navigation }/>,

        headerStyle: {
            backgroundColor: '#00bcd4'
        },
        headerTintColor: '#fff',
        })
    },
});

export default HomeDrawer = DrawerNavigator({
    Home: { 
        screen: HomeScreen_StackNavigator
    },
    Trend: {
        screen: TrendScreen_StackNavigator
    },
    Subscription: {
        screen: SubscriptionScreen_StackNavigator
    },
    History: {
        screen: HistoryScreen_StackNavigator
    },
    Liked: {
        screen: LikedScreen_StackNavigator
    },
    Logout: {
        screen: LogoutScreen_StackNavigator
    },
}, {
    drawerWidth: width(90),
    drawerLockMode: 'locked-closed',
    contentComponent: HomeMenuScreen
});

class NavDrawer extends Component{
    toggleDrawer=()=>{
   
        // console.log(this.props.navigationProps);
        
        this.props.navigationProps.toggleDrawer();
     
      }
     
      render() {
     
          return (
     
          <View style={{flexDirection: 'row', wdith:width(100)}}>
     
            <TouchableOpacity onPress={this.toggleDrawer.bind(this)} >
              <Image
                source={images.hamburger_but}
                style={styles.navHamburger}
              />
            </TouchableOpacity>
            
            <View 
                style= {styles.navBG}>
                <Text 
                    style = {styles.navTitle}>
                    Home
                </Text>
            </View>
            
           
            <TouchableOpacity style= {[styles.navButBg,{marginLeft: width(25)}]} >
              <Image
                source={images.send_but}
                style={styles.navBut}
              />
            </TouchableOpacity>

            <TouchableOpacity style= {styles.navButBg} >
              <Image
                source={images.receive_but}
                style={styles.navBut}
              />
            </TouchableOpacity>
          </View>
        
        );
      
      }
}

const styles = StyleSheet.create({
    
    MainContainer :{
        flex: 1,
        paddingTop: height(3),
        paddingLeft: width(3),
        paddingRight: width(3),
        // alignItems: 'center',
        // justifyContent: 'center',
        // backgroundColor: 'grey',
    },
    navHamburger:{
        resizeMode: 'contain', 
        width:width(5), 
        marginLeft: width(2.5)
    },
    navBG :{
        marginLeft: width(7.5),
        alignContent: 'center',
        justifyContent: 'center',
    },

    navTitle :{
        fontWeight: 'bold',
        fontSize: 20,
        color: '#fff',
    },

    navButBg:{
        marginLeft: width(2.5),
        justifyContent:'center',
        alignContent:'center',
    },

    navBut:{
        resizeMode: 'contain', 
        height:height(5), 
        width:width(20),
    },

    modalButton: {
        width: width(70),
        height: height(5),
        
    }

    
});
