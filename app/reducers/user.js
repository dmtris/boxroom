import * as types from '../const/types';

const initialState = {
  isAuthenticated: false,
  isFetching: false,
  user: {},
  errorMsg: '',
};

export default function user(state = initialState, action) {
  switch (action.type) {
    case types.LOGOUT:
      return initialState;
    default:
      return state;
  }
}
